// @dart=2.9

import 'dart:async';

import 'package:carp_background_location/carp_background_location.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:geohash/geohash.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:permission_handler/permission_handler.dart' as per;
import 'package:speed_meter/data/model/UserLocationData.dart';
import 'package:speed_meter/data/model/point.dart';
import 'package:speed_meter/domain/usecase/UpdateLocation.dart';

class SpeedMeterProvider with ChangeNotifier {
  UpdateLocation updateLocation;
  Stream<LocationDto> locationStream;
  StreamSubscription<LocationDto> locationSubscription;

  SpeedMeterProvider({this.updateLocation});

  int _speed = 0;
  String lastGeoHash = "";

  int get speed => _speed;
  Location location = Location();

  void increment() {
    _speed++;
    notifyListeners();
  }

  Future<String> getPermission() async {
    per.PermissionStatus permision = await Permission.location.status;
    if(permision != per.PermissionStatus.granted){
      permision = await Permission.location.request();
    }
    per.ServiceStatus status = await Permission.location.serviceStatus;
    if(status != per.ServiceStatus.enabled){
      return "Enable your location";
    }

    if (permision == per.PermissionStatus.granted && status == per.ServiceStatus.enabled) {
      listenLocationTracker();
      // setBackgroundMode();
    }else{
      return "Enable location Or grant permission";
    }

    // onGetCurrentLocation();
  }


  Future disposeService() async {
    await LocationManager().stop();
  }

  void setBackgroundMode() async {
    LocationManager().interval = 1;

    LocationManager().notificationTitle = 'Location';
    LocationManager().notificationMsg = 'Location.....';
    await LocationManager().start(askForPermission: false);
    locationStream = LocationManager().locationStream;

    locationSubscription = locationStream.listen((currentLocation) {
      print("in listener");
      var encoded = Geohash.encode(
          currentLocation.latitude, currentLocation.longitude,
          codeLength: 10);
      _speed = (currentLocation.speed * 3.6).floor();
      if (_speed > 220) {
        _speed = 220;
      }
      notifyListeners();
      if (lastGeoHash != encoded) {
        lastGeoHash = encoded;

        updateLocation(UserLocationData(
            time: DateTime.now().toUtc().toIso8601String(),
            point: Point(
                lat: currentLocation.latitude, long: currentLocation.longitude),
            speed: (currentLocation.speed * 3.6).floor()));
      }
    });
  }

  void listenLocationTracker() async {
    await location.enableBackgroundMode(enable: true);
    location.onLocationChanged.listen((LocationData currentLocation) {
      print(currentLocation.speed.toString());

      var encoded = Geohash.encode(
          currentLocation.latitude, currentLocation.longitude,
          codeLength: 10);
      _speed = (currentLocation.speed * 3.6).floor();
      if (_speed > 220) {
        _speed = 220;
      }
      notifyListeners();

      if (lastGeoHash != encoded) {
        lastGeoHash = encoded;

        updateLocation(UserLocationData(
            time: DateTime.now().toUtc().toIso8601String(),
            point: Point(
                lat: currentLocation.latitude, long: currentLocation.longitude),
            speed: (currentLocation.speed * 3.6).floor()));
      }
    });
  }

  void onGetCurrentLocation() async {
    LocationDto dto = await LocationManager().getCurrentLocation();
    print('Current location: $dto');
  }
}
