import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:provider/provider.dart';
import 'package:speed_meter/data/data_source/local/location_data_source.dart';
import 'package:speed_meter/data/model/UserLocationData.dart';
import 'package:speed_meter/features/map/map_screen.dart';
import 'package:speed_meter/features/speed_meter/speed_meter_provider.dart';

class SpeedMeterScreen extends StatefulWidget {
  const SpeedMeterScreen({Key? key}) : super(key: key);

  @override
  State<SpeedMeterScreen> createState() => _SpeedMeterScreenState();
}

class _SpeedMeterScreenState extends State<SpeedMeterScreen> {
  void startService(BuildContext context) async {
    try {
      // await context.read<SpeedMeterProvider>().disposeService();
      String result = await context.read<SpeedMeterProvider>().getPermission();
      if(result != null) {
        ScaffoldMessenger.of(context)
            .showSnackBar(SnackBar(content: Text(result)));
      }
    } catch (e) {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text("Errror : " + e.toString())));
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    startService(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.maxFinite,
        width: double.maxFinite,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topRight,
                end: Alignment.bottomLeft,
                colors: [Color(0xff1d212d), Color(0xff131d58)])),
        child: Center(
          child: CircularPercentIndicator(
            radius: 220.0,
            lineWidth: 20.0,
            animation: true,
            //max percent must be 75
            // if max speed is 220 -> percent = currentSpeed * 75 /(220 * 100)
            percent:
                context.watch<SpeedMeterProvider>().speed * 75 / (220 * 100),
            animateFromLastPercent: true,
            maskFilter: MaskFilter.blur(BlurStyle.solid, 5),
            startAngle: 225,
            backgroundColor: Colors.transparent,
            linearGradient: LinearGradient(colors: [
              Color(0xff86fdb6),
              Color(0xff0356fc),
            ]),
            center: GestureDetector(
              onTap: () async {
                //  List<UserLocationData> result =await LocationLocalDataSource().getLocationsData();
                // print("tr");
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    context.watch<SpeedMeterProvider>().speed.toString(),
                    style: TextStyle(
                        fontSize: 65,
                        fontFamily: "ArtegraSoft",
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 0.0),
                    child: Text(
                      "km",
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                  ),
                ],
              ),
            ),
            circularStrokeCap: CircularStrokeCap.round,
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => MapScreen()));
        },
        tooltip: 'Increment',
        child: const Icon(Icons.map),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
