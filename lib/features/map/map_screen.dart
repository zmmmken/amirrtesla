import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:speed_meter/features/map/map_provider.dart';

class MapScreen extends StatefulWidget {
  @override
  State<MapScreen> createState() => MapScreenState();
}

class MapScreenState extends State<MapScreen> {
  Completer<GoogleMapController> _controller = Completer();

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(36.310699, 59.599457),
    zoom: 10,
  );

  static final CameraPosition _kLake = CameraPosition(
      target: LatLng(36.310699, 59.599457),
      zoom: 10.151926040649414);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    context.read<MapProvider>().getInfoFromServer();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: GoogleMap(
        mapType: MapType.normal,
        markers: context
            .watch<MapProvider>()
            .data
            .map((e) => Marker(
                markerId: MarkerId(e.time),
                onTap: (){
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      content: Text(e.toString())));} ,
                position: LatLng(e.point.lat, e.point.long)))
            .toSet(),
        initialCameraPosition: _kGooglePlex,
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },
      ),
      // floatingActionButton: FloatingActionButton.extended(
      //   onPressed: _goToTheLake,
      //   label: Text('To the lake!'),
      //   icon: Icon(Icons.directions_boat),
      // ),
    );
  }

  Future<void> _goToTheLake() async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(_kLake));
  }
}
