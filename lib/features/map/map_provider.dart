import 'package:flutter/cupertino.dart';
import 'package:speed_meter/data/data_source/local/location_data_source.dart';
import 'package:speed_meter/data/data_source/remote/location_data_source.dart';
import 'package:speed_meter/data/model/UserLocationData.dart';
import 'package:speed_meter/data/repository/LocationRepositoryImpl.dart';
import 'package:speed_meter/domain/repository/LocationRepository.dart';

class MapProvider with ChangeNotifier {
  List<UserLocationData> data =[];
  void getInfoFromServer()async {
    LocationRepository repository = LocationRepositoryImpl(
        local: LocationLocalDataSource(), remote: LocationRemoteDataSource());
    data = await repository.getLocationData();
    notifyListeners();
  }
}