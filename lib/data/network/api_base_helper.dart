// @dart=2.9

import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:speed_meter/data/network/app_exception.dart';


class ApiBaseHelper {

  final String _baseUrl = "http://185.255.90.185:8080/";
  Future<dynamic> get(String url) async {
    var responseJson;
    try {
      final response = await http.get(Uri.parse(_baseUrl+ url));
      responseJson = _returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }

  Future<dynamic> post(String url,Object body) async {
    var responseJson;
    try {
      final response = await http.post(Uri.parse(_baseUrl+ url),body: jsonEncode(body),headers: {"Content-Type":"application/json"});
      responseJson = _returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }catch(e){
      print(e);
    }
    return responseJson;
  }

  dynamic _returnResponse(http.Response response) {
    switch (response.statusCode) {
      case 200:
        var responseJson = json.decode(response.body.toString());
        print(responseJson);
        return responseJson;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:
      case 403:
        throw UnauthorisedException(response.body.toString());
      case 500:
      default:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }

  }
}