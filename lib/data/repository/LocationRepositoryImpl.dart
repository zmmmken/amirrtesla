import 'package:speed_meter/data/data_source/local/location_data_source.dart';
import 'package:speed_meter/data/data_source/remote/location_data_source.dart';
import 'package:speed_meter/data/model/UserLocationData.dart';
import 'package:speed_meter/domain/repository/LocationRepository.dart';

class LocationRepositoryImpl extends LocationRepository {
  LocationLocalDataSource local;
  LocationRemoteDataSource remote;

  LocationRepositoryImpl({required this.local,required this.remote});

  @override
  Future updateLocations(List<UserLocationData> data) async {
    await local.updateLocations(data);
    List<UserLocationData> locations = await local.getLocationsData();
    if(locations.length % 15 == 0){
      bool result = await remote.updateLocations(locations);
      if(result ==true){
        print("Update Location ...........");
        await local.removeFromDataBase(locations);
      }

    }
  }

  @override
  Future<List<UserLocationData>> getLocationData() async{
    return remote.getLocationsData();
  }
}
