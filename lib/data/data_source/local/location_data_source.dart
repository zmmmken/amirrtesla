import 'dart:convert';

import 'package:localstorage/localstorage.dart';
import 'package:speed_meter/data/data_source/LocationDataSource.dart';
import 'package:speed_meter/data/model/UserLocationData.dart';
import 'package:speed_meter/data/model/user_location_storage_model.dart';

class LocationLocalDataSource extends LocationDataSource{
  final LocalStorage storage = new LocalStorage('location-list');
  String _key = "key";
  @override
  Future<bool> updateLocations(List<UserLocationData> data)async {
    if(await storage.ready) {
      try {
        List<UserLocationData> temp = await getLocationsData();
        temp.addAll(data);
        storage.setItem(_key, UserLocationStorageModel(temp).toJson());
        return true;
      } catch (e) {
        return false;
      }
    }return false;
  }

  @override
  Future<List<UserLocationData>> getLocationsData() async {
    if(await storage.ready){
      Map<String,dynamic> temp = storage.getItem(_key);
      if(temp != null){
        return Future.value(UserLocationStorageModel.fromJson(temp).data);
      }else return Future.value([]);
    }else return [];

  }

  Future<bool> removeFromDataBase(List<UserLocationData> data) async {
    List<UserLocationData> temp = await getLocationsData();
    for(UserLocationData element in data){
     temp.removeWhere((item) => element.time == item.time);
    }
    storage.setItem(_key, UserLocationStorageModel(temp).toJson());
    return true;
  }

  @override
  void updateLocation(UserLocationData data) {

  }
}