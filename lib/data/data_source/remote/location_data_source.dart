// @dart=2.9

import 'package:speed_meter/data/data_source/LocationDataSource.dart';
import 'package:speed_meter/data/model/UserLocationData.dart';
import 'package:speed_meter/data/network/api_base_helper.dart';

class LocationRemoteDataSource extends LocationDataSource {
  ApiBaseHelper _helper = ApiBaseHelper();

  @override
  Future<bool> updateLocations(List<UserLocationData> data) async {
    try {
      final response = await _helper.post('sample', data);
      print("Response : ");
      return true;
    } catch (e) {
      return false;
    }
  }

  @override
  Future<List<UserLocationData>> getLocationsData() async{
    final response = await _helper.get('sample');

    List<UserLocationData> result = [];
    List temp = response as List;
    temp.forEach((element) {
      try{
        UserLocationData locationData = UserLocationData.fromJson(element);
        result.add(locationData);
      }catch(e){
        print(e);
      }

    });
    return result;
  }
}
