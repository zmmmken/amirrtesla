import 'package:speed_meter/data/model/UserLocationData.dart';

abstract class LocationDataSource{
  Future<bool> updateLocations(List<UserLocationData> data);
  Future<List<UserLocationData>> getLocationsData();
}