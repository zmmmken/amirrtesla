import 'package:json_annotation/json_annotation.dart';
import 'package:speed_meter/data/model/UserLocationData.dart';

part 'user_location_storage_model.g.dart';

@JsonSerializable()
class UserLocationStorageModel{
    List<UserLocationData>? data;

    UserLocationStorageModel(this.data);

    factory UserLocationStorageModel.fromJson(Map<String, dynamic> json) => _$UserLocationStorageModelFromJson(json);
    Map<String, dynamic> toJson() => _$UserLocationStorageModelToJson(this);
}