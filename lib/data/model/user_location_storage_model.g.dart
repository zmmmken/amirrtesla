// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_location_storage_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserLocationStorageModel _$UserLocationStorageModelFromJson(
    Map<String, dynamic> json) {
  return UserLocationStorageModel(
    (json['data'] as List<dynamic>?)
        ?.map((e) => UserLocationData.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$UserLocationStorageModelToJson(
        UserLocationStorageModel instance) =>
    <String, dynamic>{
      'data': instance.data,
    };
