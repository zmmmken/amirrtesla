import 'package:json_annotation/json_annotation.dart';


part 'point.g.dart';

@JsonSerializable()
class Point {
  final double lat;

  @JsonKey(name: 'lang')
  final double long;

  Point({required this.lat, required this.long});


  factory Point.fromJson(Map<String, dynamic> json) => _$PointFromJson(json);
  Map<String, dynamic> toJson() => _$PointToJson(this);
}
