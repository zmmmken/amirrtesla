import 'package:json_annotation/json_annotation.dart';
import 'package:location/location.dart';
import 'package:speed_meter/data/model/point.dart';

part 'UserLocationData.g.dart';

@JsonSerializable()
class UserLocationData {
  @JsonKey(name: 'capturedAt')
  String time ="";

  @JsonKey(name: 'location')
  Point point = Point(lat:5,long:5);
  int speed = 0;

  UserLocationData(
      {required this.time,
      required this.point,
      required this.speed});

  factory UserLocationData.fromJson(Map<String, dynamic> json) => _$UserLocationDataFromJson(json);
  Map<String, dynamic> toJson() => _$UserLocationDataToJson(this);

  @override
  String toString() {
    // TODO: implement toString
    return "speed : " + speed.toString() + "  Date:" +time;
  }

}
