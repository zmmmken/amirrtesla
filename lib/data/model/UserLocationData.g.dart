// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'UserLocationData.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserLocationData _$UserLocationDataFromJson(Map<String, dynamic> json) {
  return UserLocationData(
    time: json['capturedAt'] as String,
    point: Point.fromJson(json['location'] as Map<String, dynamic>),
    speed: json['speed'] as int,
  );
}

Map<String, dynamic> _$UserLocationDataToJson(UserLocationData instance) =>
    <String, dynamic>{
      'capturedAt': instance.time,
      'location': instance.point,
      'speed': instance.speed,
    };
