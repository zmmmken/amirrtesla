// @dart=2.9

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:provider/provider.dart';
import 'package:speed_meter/arc.dart';
import 'package:location/location.dart';
import 'package:speed_meter/data/data_source/remote/location_data_source.dart';
import 'package:speed_meter/data/repository/LocationRepositoryImpl.dart';
import 'package:speed_meter/features/map/map_provider.dart';
import 'package:speed_meter/features/speed_meter/speed_meter_provider.dart';
import 'package:provider/provider.dart';
import 'package:speed_meter/features/speed_meter/speed_meter_screen.dart';
import 'package:speed_meter/domain/usecase/UpdateLocation.dart';

import 'data/data_source/local/location_data_source.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        //todo manageDependencyInjection
        ChangeNotifierProvider(
            create: (_) => SpeedMeterProvider(
                updateLocation: UpdateLocation(
                    repository: LocationRepositoryImpl(
                        local: LocationLocalDataSource(),
                        remote: LocationRemoteDataSource())))),

        ChangeNotifierProvider(create: (_) => MapProvider())
      ],
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const SpeedMeterScreen(),
    );
  }
}
