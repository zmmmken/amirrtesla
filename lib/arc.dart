import 'dart:math';

import 'package:flutter/material.dart';


class Arc extends CustomPainter {
  final double angle ;
  double doubleToAngle(double angle) => angle * pi / 180.0;

  Arc({this.angle = 210.0});

  void drawArcWithRadius(
      Canvas canvas, Offset center, double radius, double angle, Paint paint) {
    canvas.drawArc(Rect.fromCircle(center: center, radius: radius),
        doubleToAngle(-90.0), doubleToAngle(angle), true, paint);
  }

  @override
  void paint(Canvas canvas, Size size) {
    final Offset center = Offset(size.width / 2.0, size.height / 2.0);
    final double radius = size.width / 3.0;
    print("Size $size");
    print("Width ${size.width}");
    print("Size $center");
    print("Size $radius");
    Paint paint = Paint()
      ..strokeCap = StrokeCap.round
      ..strokeWidth = 20.0
      ..style = PaintingStyle.stroke
      ..color = Colors.red
      ..shader = new SweepGradient(
          colors: [
//          Color(0xFFFE7E00),
//          Color(0xFFFD0000),
            Colors.green,
            Colors.blue,
          ],
          startAngle: 0.0,
          endAngle: doubleToAngle(angle)
      ).createShader(Rect.fromCircle(center: center, radius: radius));
    drawArcWithRadius(canvas, center, radius, angle, paint);
  }
  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}