import 'package:speed_meter/data/model/UserLocationData.dart';
import 'package:speed_meter/domain/repository/LocationRepository.dart';

class UpdateLocation{

  LocationRepository repository;


  UpdateLocation({required this.repository});

  Future call(UserLocationData data)async{
    repository.updateLocations([data]);
  }
}