import 'package:speed_meter/data/model/UserLocationData.dart';

abstract class LocationRepository {
  Future updateLocations(List<UserLocationData> data);

  Future<List<UserLocationData>> getLocationData();

}
